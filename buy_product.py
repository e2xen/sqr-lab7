import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
get_inventory_size = "SELECT COALESCE(SUM(amount), 0) FROM Inventory WHERE username = %(username)s"
add_to_inventory = """
INSERT INTO Inventory(username, product, amount) 
VALUES (%(username)s, %(product)s, %(amount)s) 
ON CONFLICT (username, product) 
DO UPDATE SET amount = Inventory.amount + excluded.amount
"""


def get_connection():
    return psycopg2.connect(
        dbname="test",
        user="postgres",
        password="postgres",
        host="127.0.0.1",
        port=5432
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}

    # All operations should be inside 1 transaction to be consistent
    with get_connection() as conn:
        try:
            with conn.cursor() as cur:
                # decrease user balance
                try:
                    cur.execute(buy_decrease_balance, obj)
                    if cur.rowcount != 1:
                        raise Exception("Wrong username")
                except psycopg2.errors.CheckViolation as e:
                    raise Exception("Bad balance")
                # reduce shop stocks
                try:
                    cur.execute(buy_decrease_stock, obj)
                    if cur.rowcount != 1:
                        raise Exception("Wrong product or out of stock")
                except psycopg2.errors.CheckViolation as e:
                    raise Exception("Product is out of stock")
                # check user inventory
                cur.execute(get_inventory_size, obj)
                if amount + cur.fetchone()[0] > 100:
                    raise Exception("Inventory is full")
                # add to inventory
                cur.execute(add_to_inventory, obj)
                if cur.rowcount != 1:
                    raise Exception("Could not add product to inventory")
            conn.commit()
        except Exception as e:
            conn.rollback()
            raise e

buy_product('Alice', 'marshmello', 1)