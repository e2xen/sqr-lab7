# Extra part 1

In case of committed transaction, but failed request, 
we can add another parameter to the request, 
e.g. HTTP header `X-Request-Id` for every unique request.
It is randomly generated for every new request, and preserved for retries.
If we save request id for every committed transaction, we can
return 200 OK for the request that has already committed.

# Extra part 2

Some possible solutions:

- rollback every change in case of any failure (if possible)
- update storages unable to rollback at the end of a transaction
- update storages that are likely to fail at the start of a transaction
- regularly check between storages for consistency
- in the specific case with long operation of money withdrawal, one could mark withdrawn amount as PENDING, until the operation is fully committed 